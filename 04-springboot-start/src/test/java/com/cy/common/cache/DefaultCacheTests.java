package com.cy.common.cache;

import com.cy.pj.common.cache.DefaultCache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 代表springboot单元测试类
 */
@SpringBootTest
public class DefaultCacheTests {

    @Autowired//描述的属性由spring为其赋值(DI)
    private DefaultCache defaultCache;

    @Test
    public void test(){

        System.out.println( defaultCache);//com.cy.pj.common.cache.DefaultCache@b273a59

    }

}
