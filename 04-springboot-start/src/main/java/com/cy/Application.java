package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 在springboot工程中只能有一个类是启动类，这个类需要
 * 1.使用@SpringBootApplication注解进行描述
 * 2.此类中会有一个main方法，在main方法中初始化springboot默认配置
 * SpringApplication.run(Application.class, args); 固定写法
 * 请问此类启动时会做什么
 * 1) 通过线程调用(thread)IO从磁盘查找对应的类并将其读到内存(类加载) class
 * 2) 对读到内存中的类进行分析，哪些是交给spring管理的，由spring管理的这些类哪些是配置类
 * 3) 对spring管理的类底层要进行解析，将这些类的信息封装到指定对象
 * (Map<String,BeanDefinition>)
 * 4)Spring框架可以基于Bean的配置，借助工厂构建对象，借助Map存储对象管理对象
 * 5) 我们需要对象时，可以直接从spring容器(IOC)中去取
 *
 *
 *
 *
 *
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
